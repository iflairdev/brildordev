<?php
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);

$_objectManager = $bootstrap->getObjectManager();

$state = $_objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

//list of products to check
//$collection = $_objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
//sku => update quantity
/*foreach ($collection as $products) {
    # code...
    $Sku = $products->getSku();
}
exit;*/
//$Sku = 3;
$products = [
    '24-MB01' => 6,
    '24-MB04' => 12,
    '24-MB03' => 60
];

echo "Starting...\n";


$_zeroQtyProducts = $_objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addFieldToFilter('sku', array_keys($products));
$_stockState = $_objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
$_stockRegistry = $_objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface');

//if any products found
if($_zeroQtyProducts) {
    echo sprintf("Found %s product(s) with a qty of 0\n", count($_zeroQtyProducts));

    foreach ($_zeroQtyProducts as $_product) {
        $_stock = $_stockState->getStockQty($_product->getId(), $_product->getStore()->getWebsiteId());
        $_sku = $_product->getSku();

        echo '<pre>'; printf("## Processing %s ##\n", $_sku);

        //do a double check quantity is 0 and product has been set to update
        //if ((int)$_stock == 0 && isset($products[$_sku])) {
            $_stockItem = $_stockRegistry->getStockItem($_product->getId());
            $_stockItem->setData('is_in_stock',1); //set updated data as your requirement
            $_stockItem->setData('qty', $products[$_sku]); //set updated quantity
            $_stockItem->save(); //save stock of item
            $_product->save();
            echo '<pre>'; printf("Product had 0 qty..updated to: %s\n", $products[$_sku]);
        //}

        echo '<pre>'; printf("## Finished processing %s ##\n", $_sku);
    }

} else {
    echo '<pre>'; printf("0 Products found with provided SKU's.\n");
}


exit("Finished.");