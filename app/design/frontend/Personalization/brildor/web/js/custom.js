define(['jquery'],function($){
	$('.link_box').click(function() {
    	var $article = $(this).find('.sub-menu');
		if (!$article.is(':visible')) {
		    $article.slideDown();  //or .fadeIn() or .show()
		} else {
		    $article.slideUp();    //or .fadeOut() or .hide()
		}
	});  
});
